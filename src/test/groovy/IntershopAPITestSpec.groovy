import com.fasterxml.jackson.databind.ObjectMapper
import org.json.JSONArray
import org.json.JSONObject
import pages.responsive.HomePage
import pages.responsive.account.*
import pages.responsive.shopping.*
import pages.responsive.checkout.*
import util.IntershopUrl;

class IntershopAPITestSpec extends BaseSpec {

    def "Place Order at Storefront"()
    {
        when: "I go to the signIn page and log in..."
        go IntershopUrl.getStorefrontB2CUrl(environmentData, currency)
        at HomePage
        pressLogIn()

        then: "navigate to Account login page"
        at AccountLoginPage

        when: "and user login into storefront "
        login user, password

        then: "... then I'm logged in."
        at AccountPage

        when: "I go to the home page, add the top selling product to the cart....."
        go IntershopUrl.getStorefrontB2CUrl(environmentData, currency)
        at HomePage
        clickTopSellingProduct(3)

        then: "... find it at the Detail Page."
        at ProductDetailPage

        when: "I add it to Cart..."
        addToCart()

        then: "... and check it."
        at CartPage

        when: "Now I will checkout..."
        checkOut()

        then:
        at CheckoutPaymentPage

        when: "select your preferred payment method"
        invoice()

        then: "...and submit on storefront"
        at CheckoutReviewPage

        when: "...agree to terms and conditions and submit the order.."
        agreeCheckBox.click()
        submitButton.click()

        and:
        at CheckoutReceiptPage
        orderNum = fetchOrderNumber()
        orderAmount = fetchOrderAmount()
        println "\nOrder is created successfully on Storefront with OrderId:\t"+orderNum

        then: "..verify that user is on checkout receipt page"
        at CheckoutReceiptPage

        where:
        currency = "USD"
    }

    def 'Check if the security token can be found'() {
        given:
        def URL = environmentData.get("ms.url")[0]
        def path = environmentData.get("ms.path")[0]
        def post = new URL(URL + path).openConnection();
        def message = environmentData.get("ms.message")[0]
        post.setRequestMethod("POST")
        post.setDoOutput(true)
        post.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")

        post.getOutputStream().write(message.getBytes("UTF-8"));
        when: "... post the request to generate security token"
        def postResponseCode = post.getResponseCode();

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> jsonMap = mapper.readValue(post.getInputStream().getText(), Map.class);
        token = jsonMap.get("access_token")
        expires_on = jsonMap.get("expires_on")
        post.disconnect()

        then: "...Token should be generated.."
        postResponseCode == 200
        assert token
        println "token is generated successfully: \n$token"
    }

    def 'Verify if SALES ORDER HEADER details could be fetched using oData '() {
        given:
        def URL = environmentData.get("fno.url")[0]
        def path = environmentData.get("fno.path.SalesOrderHeader")[0]
//        def filter = "?\$filter=CustomerRequisitionNumber%20eq%20%27$orderNum%27"
        def filter = "?\$filter=CustomerRequisitionNumber%20eq%20%2700000185%27"

        //        def filter = "?\$filter=SalesOrderNumber%20eq%20%27000021%27"
        def urlConnection = new URL(URL + path + filter).openConnection();
        urlConnection.setRequestMethod("GET")
        urlConnection.setDoOutput(true)
        urlConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8")
        urlConnection.setRequestProperty("OData-MaxVersion", "4.0")
        urlConnection.setRequestProperty("OData-Version", "4.0")
        urlConnection.setRequestProperty("Accept", "application/json;odata.metadata=none")
        urlConnection.setRequestProperty("Authorization", "Bearer ${token}")

        when: "..get the Sales Order Header details based on CustomerRequisition number..."
        def getResponseCode = urlConnection.getResponseCode();
        println "response code: $getResponseCode"
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
                urlConnection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = bufferedReader.readLine()) != null) {
            response.append(inputLine);
        }
        bufferedReader.close();
        JSONObject json = new JSONObject(response.toString()); // Convert text to object
        println(json.toString(4));
        JSONArray jsonArray = json.getJSONArray("value")
        salesOrderNum = jsonArray.getJSONObject(0).getString("SalesOrderNumber")
        println "\nSales Order Number: $salesOrderNum"

        then: "..request should be successful and sales Order number should be found"
        assert getResponseCode==200
        assert salesOrderNum
    }

    def 'Verify if SALES ORDER LINE details could be fetched using oData '() {
        given:
        def URL = environmentData.get("fno.url")[0]
        def path = environmentData.get("fno.path.SalesOrderLines")[0]
        def filter = "?\$filter=SalesOrderNumber%20eq%20%27$salesOrderNum%27"
        def urlConnection = new URL(URL + path + filter).openConnection();
        urlConnection.setRequestMethod("GET")
        urlConnection.setDoOutput(true)
        urlConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8")
        urlConnection.setRequestProperty("OData-MaxVersion", "4.0")
        urlConnection.setRequestProperty("OData-Version", "4.0")
        urlConnection.setRequestProperty("Accept", "application/json;odata.metadata=none")
        urlConnection.setRequestProperty("Authorization", "Bearer ${token}")
        when: "..get the Sales Order Line details based on sales order number..."
        Thread.sleep(1000)
        def getResponseCode = urlConnection.getResponseCode();
        println("response code: "+getResponseCode);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
                urlConnection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = bufferedReader.readLine()) != null) {
            response.append(inputLine);
        }
        bufferedReader.close();
        JSONObject json = new JSONObject(response.toString()); // Convert text to object
        println(json.toString(4));

        then:
        assert getResponseCode==200
    }
}

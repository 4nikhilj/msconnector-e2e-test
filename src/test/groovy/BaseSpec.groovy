import geb.spock.GebReportingSpec
import testdata.EnvironmentDataLoader
import geb.driver.CachingDriverFactory

class BaseSpec extends GebReportingSpec {
    protected static token
    protected static expires_on
    protected static grant_type
    protected static client_id
    protected static client_secret
    protected static resource
    protected static Map<String, List> testData
    protected static Map<String, List> environmentData
    protected static user
    protected static password
    protected static orderNum
    protected static orderAmount
    protected static salesOrderNum

    def setupSpec()
    {
        environmentData= EnvironmentDataLoader.getEnvironmentData()
        user = environmentData.get("storeFront.login.eMail")[0]
        password = environmentData.get("storeFront.login.password")[0]
    }
    def cleanupSpec()
    {
//        resetBrowser()
//        CachingDriverFactory.clearCacheAndQuitDriver()
    }
}
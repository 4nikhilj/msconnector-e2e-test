package testdata

class EnvironmentDataLoader {
    private static Map<String, List> environmentData;
    private static String fileName = "EnvironmentData";
    private static String locale = "en_US";

    private void readEnvironmentDataFromFile() {
        environmentData = new HashMap<String, List>();
        ResourceBundle environmentDataBundle =
                ResourceBundle.getBundle(fileName,
                        new Locale(locale));

        Enumeration bundleKeys = environmentDataBundle.getKeys();

        while (bundleKeys.hasMoreElements()) {
            String key = (String) bundleKeys.nextElement();
            String value = environmentDataBundle.getString(key);
            if (!value.contains("["))
                value = "[" + value + "]"

            System.out.println(key + "\t=" + value);

            environmentData.put(key.toString(), Eval.me(value))
        }
    }

    /**
     * default usage reads Environment Data ResourceBundle from <br/>
     * src/remoteTest/resources/EnvirnmentData.properties 
     * @return a Map containing the properties stored in the files read
     */
    public static Map<String, List> getEnvironmentData() {
        if (environmentData == null) {
            new EnvironmentDataLoader().readEnvironmentDataFromFile();
        }
        return environmentData;
    }
}

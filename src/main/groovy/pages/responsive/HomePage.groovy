package pages.responsive

class HomePage extends StorefrontPage {


    static url = StorefrontPage.url + "ViewHomepage-Start";

    static at = {
        //The homepage must get the class "homepage" in backoffice
        waitFor { $("body", class: "homepage").size() > 0 }
    }

    static content =
            {
                signInLink(required: false) { $("a", class: "my-account-links my-account-login") }
                catalogBar { $("ul", class: contains("navbar-nav")) }
                registerLink { $("a", class: "ish-siteHeader-myAccountUtilitiesMenu-myAccount-register") }
                logoutLink { $("a", class: "my-account-link my-account-logout") }
                topsellingProduct { ($("div#GroupTopProd a")) }

            }

    //------------------------------------------------------------
    // Page checks
    //------------------------------------------------------------

    def isSignedIn(bool) {
        bool == (signInLink.size() == 0)
    }

    //------------------------------------------------------------
    // link functions
    //------------------------------------------------------------
    def pressLogIn() {
        signInLink.click()
    }

    /**
     * Method to select top selling product.
     */

    def clickTopSellingProduct(int index)
    {
        topsellingProduct[index].click()
    }
    def clickTopSellingProduct() {
        //click on first product in the list of top selling product 
        topsellingProduct[0].click()
    }

    /**
     * Method to log out of storefront.
     */
    def pressLogOut() {
        logoutLink.click()
    }

    /**
     * Method to select a catalog.
     */
    def selectCatalog(channel) {
        $("a[data-testing-id='" + channel + "-link']").click()
    }

    def clickCategoryLink(categoryId) {
        $('[data-testing-id="' + categoryId + '-link"]').click()
    }
}



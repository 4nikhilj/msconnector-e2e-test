package pages.responsive.modules

import geb.Module

class Header extends Module {

    static content = {
        searchForm { $('form', name: 'SearchBox_Header') }
        miniCartLink (wait:true){ $('a', href: '#miniCart') }
        miniCart { $('div', id: 'miniCart') }
        productCount { $('div.product-row').size() }
        logoutLink { $("a", class: "my-account-link my-account-logout") }
    }

    def search(searchTerm) {
        searchForm.$('input', type: 'text').value searchTerm
        searchForm.$('button', name: 'search').click()
    }

    def showMiniCart() {
        waitFor(10){miniCartLink.displayed}
        miniCartLink.click();
    }

    def viewCartMiniCart() {
        waitFor { miniCart.$('a', class: 'view-cart').displayed }
        miniCart.$('a', class: 'view-cart').click()
    }

    def fetchProductCount() {
        productCount
    }

    def pressLogOut() {
        logoutLink.click()
    }
}

package pages.responsive.account

import pages.responsive.StorefrontPage

class AccountPage extends StorefrontPage {
    static url = StorefrontPage.url + "ViewUserAccount-Start";

    static at =
            {
                waitFor { contentSlot.size() > 0 }
            }

    static content =
            {
                contentSlot { $("div[data-testing-id='account-page']") }
                profileSettingsLink { $("a", href: contains("ViewProfileSettings-ViewProfile"), 1) }
                changeAddressLink { $("a", href: contains("ViewUserAddressList-List"), 1) }
                giftCardBalance { $("a", href: contains("ViewGiftCertificatesBalance-Show"), 1) }
                payment { $("a", href: contains("ViewPaymentInstrumentInfo-Edit"), 1) }
                logoutLink { $("a", class: "my-account-link my-account-logout") }
            }

    //------------------------------------------------------------
    // link functions
    //------------------------------------------------------------
    def clickProfileSettings() {
        profileSettingsLink.click()

    }

    def clickChangeAddress() {
        changeAddressLink.click()
    }


}


package pages.responsive.checkout

import pages.responsive.StorefrontPage
import pages.responsive.modules.OrderAddressSummary;

class CheckoutShippingPage extends StorefrontPage {
    static at =
            {
                waitFor { contentSlot.size() > 0 }
                checkoutShippingMethods.size() > 0
            }

    static content =
            {
                contentSlot { $('div[data-testing-id="checkout-shipping-page"]') }
                checkoutShippingMethods { $('div', class: 'shipping-methods') }
                continueButton { contentSlot.$('button', name: "continue") }
                desiredDeliveryInput { contentSlot.$('input[data-testing-id="desiredDeliveryDate"]') }
                categoryName { contentSlot.@"data-testing-id" }
                orderAddressSummary { module OrderAddressSummary }
            }
    //------------------------------------------------------------
    // link functions
    //------------------------------------------------------------
    def continueClick() {
        continueButton.click()
    }

    def fillData(desiredDelivery) {
        desiredDeliveryInput.value desiredDelivery
    }


}

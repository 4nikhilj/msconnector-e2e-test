package pages.responsive.checkout

import pages.responsive.StorefrontPage
import pages.responsive.modules.CartRow
import pages.responsive.modules.OrderSummaryRow

class CartPage extends StorefrontPage {
    static url = StorefrontPage.url + "ViewCart-View";

    static at =
            {
                waitFor { contentSlot.size() > 0 }
            }

    static content =
            {
                contentSlot { $("div[data-testing-id='cart-page']") }
                updateButton { contentSlot.$("button[data-testing-id='button-update-cart']") }
                checkoutButton { $("button", name: 'checkout') }

                checkoutButtonFastPay { $('button', text: 'CHECKOUT WITH ISH DEMO FAST PAY') }

                productCartTable { term -> module(new CartRow(productTerm: term)) }

                price {
                    $('dt', text: "Subtotal").parent().find("dd", 0).text().
                            replaceAll(',', '').find(/\d+(\.\d+)?/) as BigDecimal
                }

                OrderSummaryRow { term -> module(new OrderSummaryRow(fieldName: term)) }

                //quantityInput { $("input", class:"quantity") }

            }


    def checkOrderSummaryLine(param) {
        OrderSummaryRow(param).orderLine.size() > 0
    }

    //------------------------------------------------------------
    // Page checks
    //------------------------------------------------------------

    def checkProduct(param) {
        productCartTable(param).quantityInput.size() > 0
    }

    //Method to allow only 1 qty of top selling product only 
    def allowOnlyOneProductInCart() {
        def count = productCartTable().removeLink.size()
        if (count > 1) {
            while (count != 1) {
                productCartTable().removeLink.first().click()
                count--
            }
        }
        productCartTable().quantityInputfield.value '1'
        updateButton.click()
        true
    }

    //------------------------------------------------------------
    // link functions
    //------------------------------------------------------------
    def checkOut() {
        checkoutButton.click()
    }

    def checkOutFastPay() {
        allowOnlyOneProductInCart()
        checkoutButtonFastPay.click();
    }
}
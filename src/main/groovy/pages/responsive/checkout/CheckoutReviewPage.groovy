package pages.responsive.checkout

import pages.responsive.StorefrontPage
import pages.responsive.modules.OrderInvoiceAddressSlot
import pages.responsive.modules.OrderShippingAddressSlot
import geb.module.Checkbox

class CheckoutReviewPage extends StorefrontPage {

    static at = { waitFor { contentSlot.displayed } }

    static content = {
        contentSlot { $("[data-testing-id='checkout-review-page']") }
        agreeCheckBox { $("[id='terms-conditions-agree']").module(Checkbox) }
        submitButton { $("button", name: "sendOrder") }
        orderInvoiceAddressSlot { module OrderInvoiceAddressSlot }
        orderShippingAddressSlot { module OrderShippingAddressSlot }
        directDebitMandateCheckbox { $('[id="AcceptSEPAMandate_#PBO:ID"]') }
        recurringInformationInfo { $('[data-testing-id="recurring-information-info"]') }
        editShippingAddress { $('div', 'data-testing-id': 'address-slot-ship-to-address').$('a.pull-right.btn-tool') }
        shipToMultipleAddressesBtn { $('a', text: contains('SHIP TO MULTIPLE ADDRESSES')) }
        selectAddressBtns { $('button', title: contains('Select a different address')) }
        selectAddressBtn(required: false) { $('div.open button').next().children() }
        addressDropDownValues { selectAddressBtn.$('ul li a.opt') }
        continueButton(wait: true) { $('a', name: "continue") }
    }

    def checkTermsAndConditions() {
        agreeCheckBox.check()
    }

    def continueCheckout() {
        submitButton.click()
    }

    /*
     * method to accept the direct debit mandate
     */

    def acceptDirectDebitMandate() {
        if (directDebitMandateCheckbox.displayed)
            directDebitMandateCheckbox.value(true)
    }
    /*
     * method to ship to multiple addresses
     */

    def shipToMultipleAddresses() {
        editShippingAddress.click()
        waitFor { shipToMultipleAddressesBtn.displayed }
        shipToMultipleAddressesBtn.click()
        waitFor { selectAddressBtns.first().displayed }
        def productCount
        for (int i = 0; i < selectAddressBtns.size(); i++) {
            selectAddressBtns[i].click()
            addressDropDownValues[i].click()
            sleepForNSeconds(3)
            productCount = i+1
        }
        continueButton.click()
        waitFor {$('div.list-body').size() > 0}
        assert $('div.list-body').size() > 1 : "Error: In this scenario ${productCount} products should be shipped to ${productCount} different address"
    }

    def isRecurringInformationInfoDisplayed() {
        return recurringInformationInfo.displayed
    }
}

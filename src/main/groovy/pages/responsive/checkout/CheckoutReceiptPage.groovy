package pages.responsive.checkout

import pages.responsive.StorefrontPage
import pages.responsive.modules.*

class CheckoutReceiptPage extends StorefrontPage {
    static at = {
        waitFor(message: "Checkout Receipt Page was not found") { contentSlot.displayed }
    }

    static content = {
        contentSlot { $('[data-testing-id="checkout-receipt-page"]') }
        orderInvoiceAddressSlot { module OrderInvoiceAddressSlot }
        orderShippingAddressSlot { module OrderShippingAddressSlot }
        myAccountLink { $('[data-testing-id="myaccount-link"]') }
        orderNumber { contentSlot.$("span", text: 'Your order number is:').next().first().text() }
        orderAmount { $('dd[class="total-price"]').text().split(" ").last() }
    }

    def openMyAccount() {
        myAccountLink.click()
    }

    def fetchOrderNumber() {
        orderNumber
    }

    def fetchOrderAmount() {
        orderAmount
    }
}

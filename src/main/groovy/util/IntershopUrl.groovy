package util;

class IntershopUrl {

    public static String getStorefrontB2CUrl(Map<String, List> environmentData, currency) {
        environmentData.get("intershop.protocol")[0] + "://" +
                environmentData.get("intershop.domain")[0] + ":" +
                environmentData.get("intershop.port")[0] +
                environmentData.get("intershop.path.B2C")[0] +
                currency +
                environmentData.get("intershop.resource")[0]
    }

    public static String getStorefrontB2CUrl(Map<String, List> environmentData) {
        environmentData.get("intershop.protocol")[0] + "://" +
                environmentData.get("intershop.domain")[0] + ":" +
                environmentData.get("intershop.port")[0] +
                environmentData.get("intershop.path.B2C")[0] +
                environmentData.get("intershop.resource")[0]
    }

    public static String getStorefrontB2BUrl(Map<String, List> environmentData, currency) {
        environmentData.get("intershop.protocol")[0] + "://" +
                environmentData.get("intershop.domain")[0] + ":" +
                environmentData.get("intershop.port")[0] +
                environmentData.get("intershop.path.B2B")[0] +
                currency +
                environmentData.get("intershop.resource")[0]
    }

    public static String getStorefrontB2BUrl(Map<String, List> environmentData) {
        environmentData.get("intershop.protocol")[0] + "://" +
                environmentData.get("intershop.domain")[0] + ":" +
                environmentData.get("intershop.port")[0] +
                environmentData.get("intershop.path.B2B")[0] +
                environmentData.get("intershop.resource")[0]
    }

    public static String getBackOfficeUrl(Map<String, List> environmentData) {
        environmentData.get("intershop.protocol")[0] + "://" +
                environmentData.get("intershop.domain")[0] + ":" +
                environmentData.get("intershop.port")[0] +
                environmentData.get("intershop.path.BackOffice")[0]
    }
}
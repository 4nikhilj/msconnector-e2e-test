import org.openqa.selenium.firefox.FirefoxBinary
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.firefox.FirefoxOptions
import io.github.bonigarcia.wdm.WebDriverManager;

//def gebEnv = System.getProperty("geb.env");
//def webDriverDir = System.getProperty("webDriverDir")
//def webDriverExec = new File(webDriverDir, System.getProperty("webDriverExec")).absolutePath

waiting {
    // max request time in seconds
    timeout = 50
    includeCauseInMessage = true
    retryInterval = 0.5

    //"named" timeout configurations
    slow { timeout = 10 }
    slower { timeout = 20 }
}

reportsDir = 'target/geb-reports'


//System.setProperty("webdriver.gecko.driver", 'src/main/resources/geckodriver.exe')
WebDriverManager.firefoxdriver().setup();

driver = {
    FirefoxBinary firefoxBinary = new FirefoxBinary()
    firefoxBinary.addCommandLineOptions("--headless")
    FirefoxOptions options = new FirefoxOptions()
    options.setBinary(firefoxBinary)
    driver= new FirefoxDriver(options)
//    def driver = new FirefoxDriver()
    driver.manage().window().maximize()
    return driver
}









